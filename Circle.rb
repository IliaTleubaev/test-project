class Circle
  attr_accessor :radius,:centerX,:centerY
  def initialize(radius,centerX,centerY)
    @radius = radius
    @centerX = centerX
    @centerY = centerY
  end
  def getMinX(minX)
    localminX = @centerX - @radius
    if(localminX < minX)
     return localminX
    end
    return minX
  end

  def getMinY(minY)
    localminY = @centerY - @radius
    if(localminY < minY)
      return localminY
    end
    return minY
  end

  def getMaxX(maxX)
    localmaxX = @centerX + @radius
    if(localmaxX > maxX)
      return localmaxX
    end
    return maxX
  end

  def getMaxY(maxY)
    localmaxY = @centerY + @radius
    if(localmaxY > maxY)
      return localmaxY
    end
    return maxY
  end
end
