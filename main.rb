require_relative 'ResponserHttp.rb'
require_relative 'Circle.rb'
require_relative 'Points.rb'
require_relative 'Error.rb'
require 'json'

include ResponserHttp

API_KEY_STRING = "cRuPdopsLI6U8jlF+njwIZFB9lSXAn8ryk2QlpXyYULIPkBExL9gWayvdA4DhLpwXsm9HFCm2R0Z/V6RqdkSQw=="


def tryCorrectError()
  errorRequest = ResponserHttp.getRequest()
  isCorrectResponseError = isCorrectResponse(errorRequest)
  if(!isCorrectResponseError)
    error = getErrorMessage(errorRequest)
    puts("#{error.message} code:#{error.code}")
  end
end

def main()
  arrayCircles=[]
  jsonArray = ResponserHttp.getTask(API_KEY_STRING)
  is_correct_response = isCorrectResponse(jsonArray)
  if(is_correct_response)
    arrayJsonClass = parseJson(jsonArray)
    for i in 0..arrayJsonClass.length() - 1
      arrayCircles[i] = getRectangleCoordinate(arrayJsonClass[i])
    end
    tryCorrectCoordinateRect = checkResult(arrayCircles)
    is_correct_response = isCorrectResponse(tryCorrectCoordinateRect)
    if(is_correct_response)
      getCorrectResultCoordinate(tryCorrectCoordinateRect)
    else
      error = getErrorMessage(jsonArray)
      puts("#{error.message} code:#{error.code}")
    end
  else
    error = getErrorMessage(jsonArray)
    puts("#{error.message} code:#{error.code}")
  end
end


def isCorrectResponse(res)
  data = JSON.parse(res)
  error = data['error']!=nil
  return !error
end

def getErrorMessage(res)
  data = JSON.parse(res)
  errorJson = data['error']
  error = Error.new(errorJson['code'],errorJson['message'])
  return error
end

def  parseJson (res)
  arrayClassCircle =[]
  data = JSON.parse(res)
  len =  data['result'].each { |x|  arr = Array.new()
    a = x.map {|circle| Circle.new(circle['radius'],circle['x'],circle['y'])}
  arr.push(a)}
  for i in 0..len.length() - 1
    circles =  len[i].map {|circle| Circle.new(circle['radius'],circle['x'],circle['y'])}
    arrayClassCircle.push(circles)
  end
  return arrayClassCircle
end

def getRectangleCoordinate(data)
   minX = data[0].centerX
   minY = data[0].centerY
   maxX = data[0].centerX
   maxY = data[0].centerY
 for i in 0..data.length() - 1
   minX = data[i].getMinX(minX)
   minY = data[i].getMinY(minY)
   maxX = data[i].getMaxX(maxX)
   maxY = data[i].getMaxY(maxY)
 end
   left_bottom = Point.new(minX,minY)
   right_top = Point.new(maxX,maxY)
   rect = PointsRectangle.new(left_bottom,right_top)
  return rect
end

def checkResult(arrayJson)
  res = ResponserHttp.checkResults(API_KEY_STRING,arrayJson)
  return res
end
def getCorrectResultCoordinate(res)
    data = JSON.parse(res)
    elemsOfTry = data['result']
    puts(elemsOfTry.to_s)
end
# tryCorrectError()
main()


